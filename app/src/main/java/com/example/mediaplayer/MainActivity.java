package com.example.mediaplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    private Button Botao;
    private MediaPlayer Midia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Botao = findViewById(R.id.BotaoID);
        Midia = MediaPlayer.create(MainActivity.this, R.raw.musica);

        Botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(Midia.isPlaying()){
                   Pausarmusica();
                }
               else{Tocarmusica();
                }
            }
        });
    }
    private void Tocarmusica(){
        Midia.start();
        Botao.setText("Pause");
    }
    private void Pausarmusica(){
        Midia.pause();
        Botao.setText("Tocar");
    }

    @Override
    protected void onDestroy() {

        if(Midia != null && Midia.isPlaying()){
            Midia.stop();
            Midia.release();
            Midia = null;

        }

        super.onDestroy();

    }
}